# FRUB

_FRUB_ (_Finding Repeats Using BLAST_) is a command line tool written in [Bash](https://www.gnu.org/software/bash/) to search for repeat regions within genome sequences.
Initially implemented to explore repeat regions within phage genomes, _FRUB_ can also be used with larger genomes (e.g. bacteria).

_FRUB_ runs on UNIX, Linux and most OS X operating systems.


## Dependencies

You will need to install the required programs listed in the following table, or to verify that they are already installed with the required version.

<div align="center">

| program                                      | package                                                 | version     | sources                                                                                                   |
|:-------------------------------------------- |:-------------------------------------------------------:| -----------:|:--------------------------------------------------------------------------------------------------------- |
| [_gawk_](https://www.gnu.org/software/gawk/) | -                                                       | > 4.0.0     | [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)                                                      |
| _makeblastdb_ <br> _blastn_                  | [blast+](https://www.ncbi.nlm.nih.gov/books/NBK279690/) | &ge; 2.10.1 | [ftp.ncbi.nlm.nih.gov/blast/executables/blast+](https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/)   |

</div>


## Installation and execution

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/FRUB.git
```

Give the execute permission to the file `FRUB.sh`:
```bash
chmod +x FRUB.sh
```

Execute _FRUB_ with the following command line model:
```bash
./FRUB.sh [options]
```

If at least one of the required program (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), _FRUB_ will exit with an error message.
When running _FRUB_ without option, a documentation should be displayed; otherwise, the name of the missing program is displayed before exiting.
In such a case, edit the file `FRUB.sh` and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS` (approximately lines 80-115).


## Usage

Run _FRUB_ without option to read the following documentation:

```
 USAGE: FRUB.sh -i <fasta> [-g <gff3>] [-l <lgt>] [-s <sim>] [-c <cutoff>] [-h]

 OPTIONS:
  -i <file>   FASTA-formatted input file name (mandatory)
  -g <file>   GFF3-formatted annotation file associated to the FASTA input file 
              (default: none)
  -l <int>    minimum pairwise alignment length (default: 100)
  -s <int>    minimum percentage of nucleotide identity between similar regions
              (default: 95)
  -c <int>    when specified,  any repeat pair overlapping  with both the first 
              and last c bases will be discarded (default: 0)
  -h          prints this help and exits
```


## Notes

* In brief, _FRUB_ runs different _blastn_ similarity searches of the input sequence against itself (each using different BLAST parameters). All query/subject alignments of length or with nucleotide similarity percent smaller that the specified cutoffs (options `-l` and `-s`, respectively) are discarded. When two distinct query+subject regions overlap by at least 50%, the shorter one (i.e. alignment length) is also discarded. Finally, the remaining query+subject regions are sorted and printed in tab-delimited format.

* When the option `-c` is set with an integer value _c_, every repeat pair that overlaps with both the _c_ first and last nucleotide characters are discarded.

* The BLAST similarity searches are always performed using the _blastn_ heuristics (Altschul et al. 1990, 1997) with E-value threshold = 10, word size = 7 and no base filtering/masking. However, specific match/mismatch and gap costs (see e.g. States et al. 1991) are used depending on the specified similarity percent cutoff (option `-s`). In order to find the largest putative repeat regions, more BLAST searches are carried out when the similarity percent cutoff is low, e.g. from one to nine BLAST searches for option `-s` varying from 100 to 50, respectively.

* The output tab-delimited fields are:<br>
&nbsp;01: **Qid**, query sequence name<br>
&nbsp;02: **Qstart**, start index of the query region<br>
&nbsp;03: **Qend**, end index of the query region<br>
&nbsp;04: **Sid**, subject sequence name<br>
&nbsp;05: **Sstart**, start index of the subject region<br>
&nbsp;06: **Send**, end index of the subject region<br>
&nbsp;07: **Sstrand**, subject index strand<br>
&nbsp;08: **QSlgt**, length of the query/subject alignment<br>
&nbsp;09: **QSident**, percentage of nucleotide identity in query/subject alignment<br>
&nbsp;10: **Qseq**, aligned query region<br>
&nbsp;11: **Sseq**, aligned subject region<br>
&nbsp;12: **Qannot**, annotation(s) associated to the query region (when option `-g` is set)<br>
&nbsp;13: **Sannot**, annotation(s) associated to the subject region (when option `-g` is set)

* Tab-delimited results can be saved in a tsv file by ending the _FRUB_ command line with e.g. `> results.tsv`. Such a file can be easily edited using any spreadsheet tool.


## Example

Run the following command lines to download the complete genome of [_Escherichia_ phage momo ](https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=2696424) in FASTA format ([MN85058088](https://www.ncbi.nlm.nih.gov/nuccore/MN850580); 88,168 bps), as well as its associated annotations in GFF3 format:

```bash
FTPURL=https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/010/120/555/GCA_010120555.1_ASM1012055v1/;
wget -q -O - $FTPURL/GCA_010120555.1_ASM1012055v1_genomic.fna.gz | gunzip -c > MN850580.fna
wget -q -O - $FTPURL/GCA_010120555.1_ASM1012055v1_genomic.gff.gz | gunzip -c > MN850580.gff
```
<br>

Running _FRUB_ with default options (similarity cutoff: 95%, length cutoff: 100), i.e.

```bash
FRUB.sh -i MN850580.fna
```
leads to no results:

```
#Qid  Qstart  Qend  Sid  Sstart  Send  Sstrand  QSlgt  QSident  Qseq  Sseq
```

<br>

When lowering the length cutoff to 80, i.e.

```bash
FRUB.sh -i MN850580.fna -l 80
```
one obtains three tab-delimited results:

```
#Qid       Qstart Qend  Sid        Sstart Send  Sstrand QSlgt QSident Qseq                                                                                                Sseq
MN850580.1 1050   1129  MN850580.1 87825  87904 +       80    95.000  GTATGGGTAGTTAGTCTCAACTACATAAAAATGAGACTTACCTTATTAAAAGGTCTTTAAAGAGGGTCTTTTAATGAGGT                    GTATGGGTAGTTAGTCTCAGCTACATTAAAATGAGACTTACCTTTTTAAAAGGTCTTTAAAGAGGGTCTTTTAATAAGGT
MN850580.1 47254  47348 MN850580.1 50069  50163 +       95    95.789  TCTCCAGTTTAGGATTTAGTGGTAAAGCTCTTTATCTGGGGCTTACTTTATCGAACTATAAGCCCCTTGTAAAGTACTTTATTTGTTATTTTTGA     TCTCCAATTTAGGATTTAGTGGTAAAGCTCTTTATCTGGGGCTTACTTTATCGAACTATAAGCCCCTTGTAAAGTACTTTATCAGTTAATTTTGA
MN850580.1 48031  48129 MN850580.1 50051  50149 +       99    97.980  TAATAGCCATTTTTAAGTTCTCCAGTTTAGGATTTAGTGGTAAAGCTCTTTATCTGGGGCTTACTTTATCGAACTATAAGCCCCCTGTAAAGTACTTTA TAATAGCCATTTTTAAGTTCTCCAATTTAGGATTTAGTGGTAAAGCTCTTTATCTGGGGCTTACTTTATCGAACTATAAGCCCCTTGTAAAGTACTTTA
```

<br>

Note that it is possible to select some fields using _cut_ for better reading. For example, to discard **Sid**, **Qseq** and **Sseq** (fields 4, 10 and 11), running the following command line:

```bash
FRUB.sh -i MN850580.fna -l 80  |  cut -f1-3,5-9
```
leads to:

```
#Qid       Qstart Qend   Sstart Send  Sstrand QSlgt QSident
MN850580.1 1050   1129   87825  87904 +       80    95.000
MN850580.1 47254  47348  50069  50163 +       95    95.789
MN850580.1 48031  48129  50051  50149 +       99    97.980
```

<br>

Using option `-c` can be useful to avoid repeat pairs that are too close to the respective sequence ends:

```bash
FRUB.sh -i MN850580.fna -l 80 -c 1100  |  cut -f1-3,5-9
```

```
#Qid       Qstart Qend   Sstart Send  Sstrand QSlgt QSident
MN850580.1 47254  47348  50069  50163 +       95    95.789
MN850580.1 48031  48129  50051  50149 +       99    97.980
```

<br>

Finally, specifying an annotation file (GFF3 format) enables to add two last tab-delimited fields (12 and 13) containing information about the putative repeat regions:
```bash
FRUB.sh -i MN850580.fna -g MN850580.gff -l 80 -c 1100  |  cut -f1-3,5-9,12-13
```

```
#Qid       Qstart Qend   Sstart Send  Sstrand QSlgt QSident Qannot                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       Sannot
MN850580.1 47254  47348  50069  50163 +       95    95.789  CDS;MN850580.1:47332-47535;ID=cds-QHR67239.1;Parent=gene-momo_64;Dbxref=NCBI_GP:QHR67239.1;Name=QHR67239.1;Note=PVOGs: VOG1509%3B Hypothetical protein;gbkey=CDS;locus_tag=momo_64;product=hypothetical protein;protein_id=QHR67239.1;transl_table=11                                                                                                                                                                                                                                                        CDS;MN850580.1:50150-50530;ID=cds-QHR67246.1;Parent=gene-momo_71;Dbxref=NCBI_GP:QHR67246.1;Name=QHR67246.1;Note=PVOGs: VOG1373%3B Hypothetical protein;gbkey=CDS;locus_tag=momo_71;product=hypothetical protein;protein_id=QHR67246.1;transl_table=11
MN850580.1 48031  48129  50051  50149 +       99    97.980  CDS;MN850580.1:47636-48040;ID=cds-QHR67240.1;Parent=gene-momo_65;Dbxref=NCBI_GP:QHR67240.1;Name=QHR67240.1;Note=PVOGs: VOG4921%3B Hypothetical protein;gbkey=CDS;locus_tag=momo_65;product=hypothetical protein;protein_id=QHR67240.1;transl_table=11|CDS;MN850580.1:48127-48399;ID=cds-QHR67241.1;Parent=gene-momo_66;Dbxref=NCBI_GP:QHR67241.1;Name=QHR67241.1;Note=PVOGs: VOG1374%3B Hypothetical protein;gbkey=CDS;locus_tag=momo_66;product=hypothetical protein;protein_id=QHR67241.1;transl_table=11  CDS;MN850580.1:49803-50060;ID=cds-QHR67245.1;Parent=gene-momo_70;Dbxref=NCBI_GP:QHR67245.1;Name=QHR67245.1;Note=Hypothetical protein;gbkey=CDS;locus_tag=momo_70;product=hypothetical protein;protein_id=QHR67245.1;transl_table=11
```

One can therefore observe that the different repeat regions are associated to the five coding sequences [QHR67239](https://www.ncbi.nlm.nih.gov/protein/QHR67239), [QHR67240](https://www.ncbi.nlm.nih.gov/protein/QHR67240), [QHR67241](https://www.ncbi.nlm.nih.gov/protein/QHR67241), [QHR67245](https://www.ncbi.nlm.nih.gov/protein/QHR67245) and [QHR67246](https://www.ncbi.nlm.nih.gov/protein/QHR67246).
As the second query region (MN850580:48031-48129) spans two coding sequences (i.e. MN850580:47636-48040 and MN850580:48127-48399), the two corresponding annotations (field Qannot) are separated by a vertical bar (`|`).


## References

Altschul SF, Gish W, Miller W, Myers EW, Lipman DJ (1990) _Basic local alignment search tool_. **Journal of Molecular Biology**, 215(3):403-410. [doi:10.1016/S0022-2836(05)80360-2](https://www.sciencedirect.com/science/article/pii/S0022283605803602)

Altschul SF, Madden TL, Schäffer AA, Zhang J, Zhang Z, Miller W, Lipman DJ (1997) _Gapped BLAST and PSI-BLAST: a new generation of protein database search programs_. **Nucleic Acids Research**, 25(17):3389-3402. [doi:10.1093/nar/25.17.3389](https://doi.org/10.1093/nar/25.17.3389)

States DJ, Gish W, Altschul SF (1991) _Improved sensitivity of nucleic acid database searches using application-specific scoring matrices_. **Methods**, 3(1):66-70. [doi:10.1016/S1046-2023(05)80165-3](https://www.sciencedirect.com/science/article/pii/S1046202305801653)
