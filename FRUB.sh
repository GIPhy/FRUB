#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  FRUB: Finding Repeats Using BLAST                                                                         #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2022-2024 Institut Pasteur"                                                      #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
#            4888888883                                                                                      #
#         48800007   4003 1                                                                                  #
#      4880000007   400001 83        101  100    01   4000009  888888888 101 888888888 08    80 888888888    #
#     4000000008    8000001 83       181  10101  01  601     1    181    181    181    08    80    181       #
#    40000000008    8000001 803      181  10 101 01    60003      181    181    181    08    80    181       #
#   100888880008    800007 60003     181  10  10101  4     109    181    181    181    68    87    181       #
#   81     68888    80887 600008     101  10    001   0000007     101    101    101     600009     101       #
#   808883     1    887  6000008                                                                             #
#   8000000003         480000008                                                                             #
#   600000000083    888000000007     10000000     40      4000009  888888888 10000000  08    80  1000000     #
#    60000000008    80000000007      180    39   4000    601     1    181    10        08    80  10    39    #
#     6000000008    8000000007       18000007   47  00     60003      181    1000000   08    80  1000007     #
#      680000008    800000087        180       40000000  4     109    181    10        68    87  10   06     #
#        6888008    8000887          100      47      00  0000007     101    10000000   600009   10    00    #
#            688    8887                                                                                     #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.2                                                                                                #
# + implementing a new criterion for the option -c                                                           #
# + improved result sorting                                                                                  #
# + tested using blast+ v2.16.0                                                                              #
# + corrected trap                                                                                           #
#                                                                                                            #
# VERSION=1.1-240405ac                                                                                       #
# + updating code with trap                                                                                  #
# + tested using blast+ v2.15.0                                                                              #
#                                                                                                            #
# VERSION=1.0.220121ac                                                                                       #
# + tested using blast+ v2.10.1                                                                              #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ==============================                                                                             #
# = STATIC FILES AND CONSTANTS =                                                                             #
# ==============================                                                                             #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# SUMMARY: gawk/5.0.1  blast+/2.16.0                                                                         #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  [ ! $(command -v $GAWK_BIN) ] && echoxit "no $GAWK_BIN detected" ;
  GAWK_STATIC_OPTIONS="";     
  GAWK="$GAWK_BIN $GAWK_STATIC_OPTIONS";
  TAWK="$GAWK -F\\t";
#                                                                                                            #
# -- makeblastdb ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  MBDB_BIN=makeblastdb;
  [ ! $(command -v $MBDB_BIN) ] && echoxit "no $MBDB_BIN detected" ;
  MBDB_STATIC_OPTIONS="-dbtype nucl -input_type fasta";     
  MBDB="$MBDB_BIN $MBDB_STATIC_OPTIONS";
#                                                                                                            #
# -- blastn -----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BLASTN_BIN=blastn;
  [ ! $(command -v $BLASTN_BIN) ] && echoxit "no $BLASTN_BIN detected" ;
  BLASTN_STATIC_OPTIONS="-task blastn -word_size 7 -evalue 10 -max_target_seqs 100000 -dust no -soft_masking false";     
  BLASTN="$BLASTN_BIN $BLASTN_STATIC_OPTIONS";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echoxit() ============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = randfile =============================================================================================   #
#   creates and returns a random file name that does not exist from the specified basename $1                #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m FRUB v$VERSION                             $COPYRIGHT\033[0m";
  cat <<EOF

 USAGE: FRUB.sh -i <fasta> [-g <gff3>] [-l <lgt>] [-s <sim>] [-c <cutoff>] [-h]

 OPTIONS:
  -i <file>   FASTA-formatted input file name (mandatory)
  -g <file>   GFF3-formatted annotation file associated to the FASTA input file 
              (default: none)
  -l <int>    minimum pairwise alignment length (default: 100)
  -s <int>    minimum percentage of nucleotide identity between similar regions
              (default: 95)
  -c <int>    when specified,  any repeat pair overlapping  with both the first 
              and last c bases will be discarded (default: 0)
  -h          prints this help and exits

EOF
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export LC_ALL=C;

FASTA="$NA"; # infile            -i
GFF3="$NA";  # gff3 file         -g
MINLGT=100;  # min alignment lgt -l
MINSIM=95;   # min %similarity   -s
CUTOFF=0;    # end cutoff        -c

while getopts i:g:l:s:c:h option
do
  case $option in
  i)  FASTA=$OPTARG   ;;
  g)  GFF3=$OPTARG    ;;
  l)  MINLGT=$OPTARG  ;;
  s)  MINSIM=$OPTARG  ;;
  c)  CUTOFF=$OPTARG  ;;
  h)  mandoc ; exit 0 ;;
  \?) mandoc ; exit 1 ;;
  esac
done

if [ "$FASTA" == "$NA" ]
then                         echoxit "[ERROR] file not specified (option -i): $FASTA" ;
else
  [ ! -e $FASTA ]         && echoxit "[ERROR] file not found (option -i): $FASTA" ;
  [   -d $FASTA ]         && echoxit "[ERROR] not a file (option -i): $FASTA" ;
  [ ! -s $FASTA ]         && echoxit "[ERROR] empty file (option -i): $FASTA" ;
  [ ! -r $FASTA ]         && echoxit "[ERROR] no read permission (option -i): $FASTA" ;
fi
if [ "$GFF3" != "$NA" ]
then
  [ ! -e $GFF3 ]          && echoxit "[ERROR] file not found (option -g): $GFF3" ;
  [   -d $GFF3 ]          && echoxit "[ERROR] not a file (option -g): $GFF3" ;
  [ ! -s $GFF3 ]          && echoxit "[ERROR] empty file (option -g): $GFF3" ;
  [ ! -r $GFF3 ]          && echoxit "[ERROR] no read permission (option -g): $GFF3" ;
fi
[[ $MINLGT =~ ^[0-9]+$ ]] || echoxit "[ERROR] incorrect value (option -l): $MINLGT" ;
 [ $MINLGT -lt 30 ]       && echoxit "[ERROR] too short (option -l): $MINLGT" ;
[[ $MINSIM =~ ^[0-9]+$ ]] || echoxit "[ERROR] incorrect value (option -s): $MINSIM" ;
 [ $MINSIM -lt 50 ]       && echoxit "[ERROR] too small (option -s): $MINSIM" ;
 [ $MINSIM -gt 100 ]      && echoxit "[ERROR] too large (option -s): $MINSIM" ;
[[ $CUTOFF =~ ^[0-9]+$ ]] || echoxit "[ERROR] incorrect value (option -c): $CUTOFF" ;


##############################################################################################################
####                                                                                                      ####
#### TMP FILES AND TRAP                                                                                   ####
####                                                                                                      ####
##############################################################################################################

TMPDIR=${TMPDIR:-/tmp};

INFILE=$(randfile $TMPDIR/$(basename ${FASTA%.*}));  # rewritten input file
DB=$(randfile $TMPDIR/$(basename ${FASTA%.*}));      # formatted input file
OUT=$(randfile $TMPDIR/$(basename ${FASTA%.*}));     # BLASTN results
FTMP=$(randfile $TMPDIR/$(basename ${FASTA%.*}));    # rewritten GFF3 file

finalize() { rm -f $INFILE $DB $DB.n* $OUT $FTMP ; }
trap 'finalize ; exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;


##############################################################################################################
####                                                                                                      ####
#### FORMATING FASTA                                                                                      ####
####                                                                                                      ####
##############################################################################################################

$TAWK '!/^>/  {s=s$0;next}
       (s!=""){print s;s=""}
              {print}
       END    {print s}' $FASTA > $INFILE ;

$MBDB -in $INFILE -out $DB  &>/dev/null ;


##############################################################################################################
####                                                                                                      ####
#### RUNNING BLASTN                                                                                       ####
####                                                                                                      ####
##############################################################################################################

# === TECHNICAL NOTES =============================================================================== 
#
#   Both blastn parameters -reward and -penalty are set according to the MINSIM parameter
#   (for more details, see e.g. States et al. 1991 www.stateslab.org/publications/methods.pdf)
#
#   The whole range of blastn parameters are summarized here: www.ncbi.nlm.nih.gov/books/NBK279684/
#
#   Of note, for each combination (penalty,reward), the most stringent gap cost parameters are selected
#
# MINSIM lower bound     BLASTN options                                      |rwd/pen|  gap costs (open/extend)
# ------------------     ------------------------------------------------    ---------  --------------------------------------
# [ $MINSIM -le 55 ]  && "-reward 3 -penalty -2 -gapopen 5  -gapextend 5";   1.50       5/5
# [ $MINSIM -le 65 ]  && "-reward 5 -penalty -4 -gapopen 10 -gapextend 6";   1.25       10/6, 8/6
# [ $MINSIM -le 73 ]  && "-reward 1 -penalty -1 -gapopen 3  -gapextend 2";   1.00       3/2, 2/2, 1/2, 0/2, 4/1, 3/1, 2/1
# [ $MINSIM -le 80 ]  && "-reward 4 -penalty -5 -gapopen 6  -gapextend 5";   0.80       6/5, 5/5, 4/5, 3/5
# [ $MINSIM -le 85 ]  && "-reward 3 -penalty -4 -gapopen 6  -gapextend 3";   0.75       6/3, 5/3, 4/3, 6/2, 5/2, 4/2
# [ $MINSIM -le 90 ]  && "-reward 2 -penalty -3 -gapopen 4  -gapextend 4";   0.66       4/4, 2/4, 0/4, 3/3, 6/2, 5/2, 4/2, 2/2  * default: 5/2
# [ $MINSIM -le 95 ]  && "-reward 1 -penalty -2 -gapopen 2  -gapextend 2";   0.50       2/2, 1/2, 0/2, 3/1, 2/1, 1/1
# [ $MINSIM -le 98 ]  && "-reward 2 -penalty -5 -gapopen 2  -gapextend 4";   0.40       2/4, 0/4, 4/2, 2/2
# [ $MINSIM -le 100 ] && "-reward 1 -penalty -3 -gapopen 2  -gapextend 2";   0.33       2/2, 1/2, 0/2, 2/1, 1/1 

BOPT="-query $INFILE -db $DB -perc_identity $MINSIM";

#                                                                                   output fields: 1----- 2--- 3----- 4--- 5----- 6----- 7--- 8----- 9----- 10-- 11--
{ [ $MINSIM -le  55 ] && $BLASTN $BOPT  -reward 3 -penalty -2 -gapopen 5  -gapextend 5  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  65 ] && $BLASTN $BOPT  -reward 5 -penalty -4 -gapopen 10 -gapextend 6  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  73 ] && $BLASTN $BOPT  -reward 1 -penalty -1 -gapopen 3  -gapextend 2  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  80 ] && $BLASTN $BOPT  -reward 4 -penalty -5 -gapopen 6  -gapextend 5  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  85 ] && $BLASTN $BOPT  -reward 3 -penalty -4 -gapopen 6  -gapextend 3  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  90 ] && $BLASTN $BOPT  -reward 2 -penalty -3 -gapopen 4  -gapextend 4  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  95 ] && $BLASTN $BOPT  -reward 1 -penalty -2 -gapopen 2  -gapextend 2  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le  98 ] && $BLASTN $BOPT  -reward 2 -penalty -5 -gapopen 2  -gapextend 4  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ;
  [ $MINSIM -le 100 ] && $BLASTN $BOPT  -reward 1 -penalty -3 -gapopen 2  -gapextend 2  -outfmt '6 qseqid qlen qstart qend sseqid sstart send length pident qseq sseq' ; } |

  sort -k1,1 -k8rg,8 |                                                                             ## NOTE: sorting according to alignment length (field 8; decreasing order)
    
    $TAWK -v ml=$MINLGT -v c=$CUTOFF '($3==$6&&$4==$7){next}                                       ## NOTE: discarding full sequence 
                                      ($8<ml)         {next}                                       ## NOTE: discarding short alignment, i.e. < MINLGT
                                      ($3<$6)         {ss=($6<$7)?$6:$7; se=($6<$7)?$7:$6;         ## NOTE: ($3<$6) to force left2right repeat pair sorting
                                                       if($1==$5&&$3<=c&&se>=$2-c){next}           ## NOTE: discarding pairs that are close to the ends of the same sequence (according to option -c)
                                                       h=0;
                                                       x=$3-1;while(++x<=$4)(hit[$1][x]==1)&&++h;  ## NOTE: checking that the query and subject do not overlap larger hit regions                  
                                                       x=ss-1;while(++x<=se)(hit[$5][x]==1)&&++h;                                          
                                                       if(h/($4-$3+se-ss+2)>0.5){next}                                                      
                                                       x=$3-1;while(++x<=$4)hit[$1][x]=1;          ## NOTE: storing query and subject regions, and next printing
                                                       x=ss-1;while(++x<=se)hit[$5][x]=1; 
                                                       print}' |                                        
    
      sort -k1,1 -k3g,3 |                                                                          ## NOTE: sorting according to query start (field 3; increasing order)
    
        $TAWK '{if($6<$7){ss=$6;se=$7;strand="+"}
                else     {ss=$7;se=$6;strand="-"}                                                  ## NOTE: switching the subject indices when reverse-complement
                x=$1$3$4$5""ss""se; if(x in hit)next;                                              ## NOTE: checking that the query region is not already hit
                hit[$5""ss""se""$1$3$4]; 
                print$1"\t"$3"\t"$4"\t"$5"\t"ss"\t"se"\t"strand"\t"$8"\t"$9"\t"$10"\t"$11}' > $OUT ; 

if [ "$GFF3" == "$NA" ]
then
  echo -e "#Qid\tQstart\tQend\tSid\tSstart\tSend\tSstrand\tQSlgt\tQSident\tQseq\tSseq" ; 
  cat $OUT ; 
fi


##############################################################################################################
####                                                                                                      ####
#### DEALING WITH GFF3 (IF ANY)                                                                           ####
####                                                                                                      ####
##############################################################################################################

if [ "$GFF3" != "$NA" ]
then
  $TAWK '/^#/          {next}
         (length()==0) {next} 
         ($3=="region"){next}
         ($3=="gene")  {next}
                       {x=$1$4$5; if(x in entry)next; 
                        entry[x]; 
                        print $3"\t"$1"\t"$4"\t"$5"\t"$9}' $GFF3 |
    sort -k1,1 -k3g,3 > $FTMP ;

  echo -e "Qid\tQstart\tQend\tSid\tSstart\tSend\tSstrand\tQSlgt\tQSident\tQseq\tSseq\tQannot\tSannot" ; 

  while read -r qseqid qstart qend sseqid sstart send strand length pident qseq sseq
  do
    annot="$($TAWK -v qs=$qstart -v qe=$qend -v ss=$sstart -v se=$send 'BEGIN           {qout="";sout="";}
                                                                        (qs<=$4&&qe>=$3){qout=qout"|"$1";"$2":"$3"-"$4";"$5}
                                                                        (ss<=$4&&se>=$3){sout=sout"|"$1";"$2":"$3"-"$4";"$5}
                                                                        END             {print substr(qout,2)"\t"substr(sout,2)}' $FTMP)";
    echo -e "$qseqid\t$qstart\t$qend\t$sseqid\t$sstart\t$send\t$strand\t$length\t$pident\t$qseq\t$sseq\t$annot" ; 
  done < $OUT ;
fi


##############################################################################################################
####                                                                                                      ####
#### EXITING                                                                                              ####
####                                                                                                      ####
##############################################################################################################

finalize ;

exit ;

